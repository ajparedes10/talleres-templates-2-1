package estructuras;

public class MapEdge {
	/**
     * Identificador único del nodo de origen (aeropuerto de origen) del vuelo.
     */
    public final int from;

    /**
     * Identificador único del nodo final (aeropuerto de destino) del vuelo.
     */
    public final int to;

    /**
     * Distancia geográfica entre el nodo de origen y el nodo final.
     */
    public final double distance;
    
    /**
     * Identificador único de la aerolínea que opera el vuelo entre los dos nodos.
     */
    public final int airline;
 
    /**
     * Precio del vuelo correspondiente a este arco
     */
    public final double cost;
 
    
    /**
     * Constructor de un vuelo entre dos aeropuertos.
     * @param from Identificador único del nodo de origen (aeropuerto de origen) del vuelo. from >= 0.
     * @param to Identificador único del nodo final (aeropuerto de destino) del vuelo. to >= 0.
     * @param distance Distancia geográfica entre el nodo de origen y final del trayecto
     * @param airline Identificador único de la aerolínea que opera el vuelo entre los dos nodos. airline >= 0.
     * @param cost Precio del vuelo correspondiente a este arco. cost 80 >= 10000.
     */
    public MapEdge(int from, int to, double distance, int airline, double cost)
    {
        this.from = from;
        this.to = to;
        this.distance = distance;
        this.airline = airline;
        this.cost = cost;
    }
    
    /**
     * Método toString
     */
    public String toString(String airline) {
    	return "Vuelo desde: "+from+" hasta: "+to+", Operado por: "+airline+", Distancia: "+distance+"KM, Costo: "+cost;
    }
}
